import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    config = os.path.join(
        get_package_share_directory('agents_monitor'),
        'config',
        'params.yaml'
        )
    return LaunchDescription([
        Node(
            package='agents_monitor',
            executable='mocap_proxy',
            name='mocap_proxy',
            parameters = [config]
        ),
        Node(
            package='rviz2',
            namespace='',
            executable='rviz2',
            name='rviz2'
        ),
    ])