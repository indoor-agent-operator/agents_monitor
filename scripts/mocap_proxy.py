# Copyright 2021 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import datetime

import rclpy
from rclpy.node import Node
import requests
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener
import json


class MocapProxy(Node):
    def __init__(self):
        global lab_origin, server_addr, set_agents_url, get_agents_names_url, get_agents_names_url, delete_agents_url, timer_cycle_time
        super().__init__('mocap_proxy')
        self.declare_parameters(
            namespace='',
            parameters=[('server_addr', None), ('lab_origin', None), ('set_agents_url', None),
                        ('get_agents_names_url', None), ('delete_agents_url', None), ('timer_cycle_time', None)])
        self.lab_origin = self.get_parameter('lab_origin').get_parameter_value().string_value
        self.server_addr = self.get_parameter('server_addr').get_parameter_value().string_value
        self.set_agents_url = self.create_url(self.get_parameter('set_agents_url').get_parameter_value().string_value)
        self.get_agents_names_url = self.create_url(
            self.get_parameter('get_agents_names_url').get_parameter_value().string_value)
        self.delete_agents_url = self.create_url(
            self.get_parameter('delete_agents_url').get_parameter_value().string_value)
        self.timer_cycle_time = self.get_parameter('timer_cycle_time').get_parameter_value().double_value
        self.agents_names = []
        self.agents = {}
        self.delete_agents()
        self.get_agents_names()
        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, self)
        self.timer = self.create_timer(self.timer_cycle_time, self.timer)

    def create_url(self, url):
        return "http://" + self.server_addr + url

    def delete_agents(self):
        try:
            requests.post(url=self.delete_agents_url)
        except:
            self.get_logger().info('Cannot delete agents')

    def get_agents_names(self):
        try:
            agents_query_set = requests.post(url=self.get_agents_names_url)
            agents = json.loads(agents_query_set.text)
            for agent in agents:
                self.agents_names.append(agent['name'])
        except:
            self.get_logger().info('Cannot get agents names')

    def timer(self):
        self.get_agents()
        if len(self.agents) != 0:
            self.set_agents()

    def get_agents(self):
        to_frame_rel = self.lab_origin
        self.agents = {}
        agent_num = 0
        for agent_name in self.agents_names:
            try:
                from_frame_rel = agent_name
                trans_agent_frame = self.tf_buffer.lookup_transform(to_frame_rel, from_frame_rel, rclpy.time.Time())
                self.agents[str(agent_num)] = {'name': agent_name,
                                           'translation_x': trans_agent_frame.transform.translation.x,
                                           'translation_y': trans_agent_frame.transform.translation.y,
                                           'translation_z': trans_agent_frame.transform.translation.z,
                                           'rotation_x': trans_agent_frame.transform.rotation.x,
                                           'rotation_y': trans_agent_frame.transform.rotation.y,
                                           'rotation_z': trans_agent_frame.transform.rotation.z,
                                           'rotation_w': trans_agent_frame.transform.rotation.w,
                                           'parent': trans_agent_frame.header.frame_id}
                agent_num = agent_num + 1
            except:
                self.get_logger().info('Cannot get agents')

    def set_agents(self):
        try:
            requests.post(url=self.set_agents_url, data=json.dumps(self.agents, default=str))
        except:
            self.get_logger().info('Cannot set agents')


def main():
    rclpy.init()
    node = MocapProxy()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass

    rclpy.shutdown()
