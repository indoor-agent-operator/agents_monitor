# Agents Monitor

ROS package that monitors agents tf.
It receives tf, visualizes them via Rviz, and sends them to the companion server.
The server [Indoor Operator Station](https://gitlab.com/indoor-agent-operator/indoor-operator-station) is a companion project.

## Usage

In the following example, we use a parameter file located in the config directory.

```console
ros2 launch agents_monitor agents_monitor.launch.py
```

## Parameters
The parameter file should be in ROS-parameter-file format, like this:

```yaml
mocap_proxy:
  ros__parameters:

    lab_origin: "world"
    server_addr: "127.0.0.1:8000"
    set_agents_url: "/set_agents/"
    get_agents_names_url: "/get_agents_names/"
    delete_agents_url: "/delete_agents/"
    timer_cycle_time: 0.5
```

| Parameter 	| Meaning		|Default Value	|	Remark	|
| ---------		| ------------	|-------		|-------	|
| lab_origin 	| The lab origin tf		|world	|	The Agents' tfs are related to this tf	|
| server_addr 	| IP address and port of the companion server		|127.0.0.1:8000	|	Set according to companion server	|
| set_agents_url 	| The url of the companion server to send the agents' tfs to		|/set_agents/	|	Set according to companion server	|
| get_agents_names_url 	| The url of the companion server to get the agents' names from		|/get_agents_names/	|	Set according to companion server	|
| delete_agents_url 	| The url of the companion server to delete the information from the previous run		|/delete_agents/	|	Set according to companion server	|
| timer_cycle_time 	| Sample time of received tf		|0.5	|	Set roughly as half period time of agent's request	|


### ROS Dependencies

* rclpy
* tf2_ros
* launch_ros

